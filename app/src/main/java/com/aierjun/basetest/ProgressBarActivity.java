package com.aierjun.basetest;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

/**
 * Created by aierJun on 2016/7/10.
 */
public class ProgressBarActivity extends AppCompatActivity {
    private Button btn;
    private ProgressBar progressBar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progressbar);
        findView();
        initView();
    }

    private void findView() {
        btn= (Button) findViewById(R.id.btn);
        progressBar= (ProgressBar) findViewById(R.id.progressbar);
    }

    private void initView() {
        btn.setOnClickListener(onClickListener);
    }
    private View.OnClickListener onClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
           /* if (progressBar.getVisibility()==View.GONE){
                progressBar.setVisibility(View.VISIBLE);
            }else{
                progressBar.setVisibility(View.GONE);
            }*/
            int progress=progressBar.getProgress();
            progress=progress+10;
            progressBar.setProgress(progress);
        }
    };
}
