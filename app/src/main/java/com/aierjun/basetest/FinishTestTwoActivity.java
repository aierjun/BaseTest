package com.aierjun.basetest;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by aierJun on 2016/7/10.
 */
public class FinishTestTwoActivity extends AppCompatActivity{
    private Button btn;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finishtwo);
        btn= (Button) findViewById(R.id.two);
        btn.setOnClickListener(onClickListener);
    }
   private View.OnClickListener onClickListener=new View.OnClickListener() {
       @Override
       public void onClick(View view) {
          finish();
       }
   };
}
