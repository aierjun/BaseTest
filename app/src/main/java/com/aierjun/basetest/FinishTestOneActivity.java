package com.aierjun.basetest;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by aierJun on 2016/7/10.
 */
public class FinishTestOneActivity extends AppCompatActivity{
    private Button btn;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn= (Button) findViewById(R.id.go);
        btn.setOnClickListener(onClickListener);
    }
   private View.OnClickListener onClickListener=new View.OnClickListener() {
       @Override
       public void onClick(View view) {
           startActivity(new Intent(FinishTestOneActivity.this,FinishTestTwoActivity.class));
       }
   };
}
